from django.forms import ModelForm, DateTimeInput
from tasks.models import Task


class TaskForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["start_date"].widget = DateTimeInput(
            attrs={"type": "datetime-local"}
        )
        self.fields["due_date"].widget = DateTimeInput(
            attrs={"type": "datetime-local"}
        )

    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
