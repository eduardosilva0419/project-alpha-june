from django.urls import path
from .views import login_user, logout_user, signup_user
from django.shortcuts import redirect


def redirects_name(resquest):
    return redirect("home")


urlpatterns = [
    path("login/", login_user, name="login"),
    path("logout/", logout_user, name="logout"),
    path("signup/", signup_user, name="signup"),
]
