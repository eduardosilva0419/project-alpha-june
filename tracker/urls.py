from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect


def redirect_to_home(request):
    return redirect("list_projects")


def redirect_to_signup(request):
    return redirect("signup")


urlpatterns = [
    path("", redirect_to_home, name="home"),
    path("admin/", admin.site.urls),
    path("projects/", include("projects.urls")),
    path("accounts/", include("accounts.urls")),
    path("signup/", redirect_to_signup),
    path("tasks/", include("tasks.urls")),
]
